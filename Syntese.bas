Attribute VB_Name = "Syntese"
Option Explicit

Type Moy_Proj
    MoyTri As Integer
    MoyPreks As Integer
    MoyPrekc As Integer
    MoyPres As Integer
    MoyChasse As Integer
    MoyAmd As Integer
End Type

Type Moy_Jour
    jourTri As Integer
    jourPreks As Integer
    jourPrekc As Integer
    jourPres As Integer
    jourChasse As Integer
    jourAmd As Integer
End Type

Type Result
    Reussi As Integer
    enAttente As Integer
    Annule As Integer
    Echec As Integer
    Revise As Integer
    SansRetour as Integer
End Type

    
Dim Type_Tri As New TypeMissions
Dim Type_Preks As New TypeMissions
Dim Type_Prekc As New TypeMissions
Dim Type_Pres As New TypeMissions
Dim type_chasse As New TypeMissions
Dim Type_AMD As New TypeMissions
Dim Moy_Proj As Moy_Proj
Dim Moy_Jour As Moy_Jour
Dim Result As Result

Sub SynteseGeneral()

    Dim i, y As Integer
    Dim DerLig As Long
    Dim nbrMission As Integer
    Dim nbrProjet As Integer
    Dim etat_EnCour As Integer
    Dim etat_Terminer As Integer
    Dim etat_NonCommencer As Integer
    Dim etat_Cloturer As Integer
    Dim etat_StundBy As Integer
    Dim etat_NotEdit As Integer
    Dim etat_Mission As String
    Dim nbr_Proj() As String
    Dim x As Long
    Dim celul As Integer
    Dim tabDesDonnes(14, 6) As Integer
    Dim annTrait(13) As String
    Dim n As Integer
    Dim extract As Variant
  
    DerLig = Sheets("extract").Range("A65536").End(xlUp).Row 'on récupére la derniere ligne de la bdd

    extract = Sheets("extract").Range("A2:M" & DerLig).Value 'on récupére tous le tableau pour le traitement

    'Debug.Print extract(UBound(extract), 9)
    'on commence par 2015****

    annTrait(1) = "janvier"
    annTrait(2) = "février"
    annTrait(3) = "mars"
    annTrait(4) = "avril"
    annTrait(5) = "mai"
    annTrait(6) = "juin"
    annTrait(7) = "juillet"
    annTrait(8) = "août"
    annTrait(9) = "septembre"
    annTrait(10) = "octobre"
    annTrait(11) = "novembre"
    annTrait(12) = "décembre"

    For n = 1 To UBound(annTrait) - 1
        tabDesDonnes(0, 1) = 0

        ReDim nbr_Proj(DerLig, 1)
        'on calcule le nombre de projet total
        For i = 1 To UBound(extract)
            For y = 0 To UBound(nbr_Proj)
       
       
                'If Sheets("extract").Range("I" & i).Value = nbr_Proj(y, 0) Then 'extract(i,9)
                If extract(i, 9) = nbr_Proj(y, 0) Then
                    GoTo suivant
                End If
        
            Next
            x = 0
        
            Do Until nbr_Proj(x, 0) = ""
                x = x + 1
            Loop
        
            'If Format(Sheets("extract").Range("E" & i).Value, "yyyy") = annTrait(n) Then 'on vérifie l'année
            If Format(extract(i, 5), "mmmm") = annTrait(n) Then
                'nbr_Proj(x, 0) = Sheets("extract").Range("I" & i).Value
                nbr_Proj(x, 0) = extract(i, 9)
                'nbr_Proj(x, 1) = Sheets("extract").Range("G" & i).Value
                nbr_Proj(x, 1) = extract(i, 7)
            End If
        
        
suivant:

        Next
        'initialisation des données

    
        nbrProjet = 0
        nbrMission = 0
        etat_EnCour = 0
        etat_Terminer = 0
        etat_NonCommencer = 0
        etat_Cloturer = 0
        etat_StundBy = 0
        etat_NotEdit = 0
    
        Call Restart 'rinitaliser
    
        'on efface les données vide pour avoir les projet réel
    
        For x = 0 To UBound(nbr_Proj)
            If nbr_Proj(x, 0) > "0" Then
                nbrProjet = nbrProjet + 1
            End If
        Next
        
       
    
        'on calcule le nomnbre de projet de prék simple
        For x = 0 To UBound(nbr_Proj)
            select case nbr_Proj(x, 1) 
                case "APKS" 
                    Type_Preks.Projet = Type_Preks.Projet + 1
           
        'on calcule le nomnbre de projet de prek complete
       
                case "APKC" 
                    Type_Prekc.Projet = Type_Prekc.Projet + 1
            
        'on calcule le nomnbre de projet de présélection
        
                 case "AP" 
                    Type_Pres.Projet = Type_Pres.Projet + 1
            
        'on calcule le nomnbre de projet de chasse
       
                case  "ACH" 
                    type_chasse.Projet = type_chasse.Projet + 1
            
        'on calcule le nomnbre de projet de trie
        
                case "AT" 
                        Type_Tri.Projet = Type_Tri.Projet + 1
            
        'on calcule le nomnbre de projet de mise é dispo
        
                case "AMD" 
                    Type_AMD.Projet = Type_AMD.Projet + 1
           
        next
    
    
        For y = 1 To UBound(extract)
          
                            
              
            '***********************************************
            'trouver le nombre de projet par type de mission
            '***********************************************
              
            'If Format(Sheets("extract").Range("E" & y).Value, "yyyy") = annTrait(n) Then
            If Format(extract(y, 5), "mmmm") = annTrait(n) Then
                nbrMission = nbrMission + 1
         
                'Select Case Sheets("extract").Range("G" & y)
                Select Case extract(y, 7)
                    Case "AT"
                        tabDesDonnes(0, 1) = tabDesDonnes(0, 1) + 1
                        'tabDesDonnes(0, 1) = tabDesDonnes(0, 1) + 1
                        'Type_Tri.Projet = Type_Tri.Projet + Sheets("extract").Range("H" & y)
                    
                        If Not extract(y, 11) = "" Then
                            'tabDesDonnes(9, 1)
                            Moy_Proj.MoyTri = Moy_Proj.MoyTri + CInt(extract(y, 11))
                            Moy_Jour.jourTri = Moy_Jour.jourTri + CInt(extract(y, 8))
                            Debug.Print Moy_Proj.MoyTri
                        End If
                        
                        '************
                        '*les etats**
                        '************
                        
                        Select Case extract(y, 10)
                            Case ""
                                Type_Tri.Vide = Type_Tri.Vide + 1
                                
                            Case "Terminer"
                                Type_Tri.Done = Type_Tri.Done + 1
                                
                            Case "stand by"
                                Type_Tri.StandBy = Type_Tri.StandBy + 1
                                
                            Case "Clôturer"
                                Type_Tri.Cloture = Type_Tri.Cloture + 1
                                
                            Case "En Cours"
                                Type_Tri.Cour = Type_Tri.Cour + 1
                                
                            Case "Non Commencé"
                                Type_Tri.NotYet = Type_Tri.NotYet + 1
                                
                        End Select
                        
                        '**************
                        'les résultats
                        '**************
                        Select Case extract(y, 12)
                            Case "Réussi"
                                Type_Tri.Reussi = Type_Tri.Reussi + 1
                                    
                            Case "En attente"
                                Type_Tri.enAttente = Type_Tri.enAttente + 1
                                    
                            Case "Echec"
                                Type_Tri.Echec = Type_Tri.Echec + 1
                                    
                            Case "Annulé"
                                Type_Tri.Annule = Type_Tri.Annule + 1
                                    
                            Case "Révisé"
                                Type_Tri.Revise = Type_Tri.Revise + 1
                                                                
                        End Select
                            
                    Case "APKC"
                
                        Type_Prekc.Mission = Type_Prekc.Mission + 1
                        'Type_Prekc.Projet = Type_Prekc.Projet + Sheets("extract").Range("H" & y)
                    
                        If Not extract(y, 11) = "" Then
                    
                            Moy_Proj.MoyPrekc = Moy_Proj.MoyPrekc + CInt(extract(y, 11))
                            Moy_Jour.jourPrekc = Moy_Jour.jourPrekc + CInt(extract(y, 8))
                         
                        End If
                     
                        
                    
                        Select Case extract(y, 10)
                            Case ""
                                Type_Prekc.Vide = Type_Prekc.Vide + 1
                                
                            Case "Terminer"
                                Type_Prekc.Done = Type_Prekc.Done + 1
                                
                            Case "stand by"
                                Type_Prekc.StandBy = Type_Prekc.StandBy + 1
                                
                            Case "Clôturer"
                                Type_Prekc.Cloture = Type_Prekc.Cloture + 1
                                
                            Case "En Cours"
                                Type_Prekc.Cour = Type_Prekc.Cour + 1
                                
                            Case "Non Commencé"
                                Type_Prekc.NotYet = Type_Prekc.NotYet + 1
                                
                        End Select
                         
                        '**************
                        'les résultats
                        '**************
                        Select Case extract(y, 12)
                            Case "Réussi"
                                Type_Prekc.Reussi = Type_Prekc.Reussi + 1
                                    
                            Case "En attente"
                                Type_Prekc.enAttente = Type_Prekc.enAttente + 1
                                    
                            Case "Echec"
                                Type_Prekc.Echec = Type_Prekc.Echec + 1
                                    
                            Case "Annulé"
                                Type_Prekc.Annule = Type_Prekc.Annule + 1
                                    
                            Case "Révisé"
                                Type_Prekc.Revise = Type_Prekc.Revise + 1
                                                                
                        End Select
                        
                    Case "AP"
                        Type_Pres.Mission = Type_Pres.Mission + 1
                        'Type_Pres.Projet = Type_Pres.Projet + Sheets("extract").Range("H" & y)
                    
                        If Not extract(y, 11) = "" Then
                    
                            Moy_Proj.MoyPres = Moy_Proj.MoyPres + CInt(extract(y, 11))
                            Moy_Jour.jourPres = Moy_Jour.jourPres + CInt(extract(y, 8))
                         
                        End If
                    
                    
                        Select Case extract(y, 10)
                            Case ""
                                Type_Pres.Vide = Type_Pres.Vide + 1
                                
                            Case "Terminer"
                                Type_Pres.Done = Type_Pres.Done + 1
                                
                            Case "stand by"
                                Type_Pres.StandBy = Type_Pres.StandBy + 1
                                
                            Case "Clôturer"
                                Type_Pres.Cloture = Type_Pres.Cloture + 1
                                
                            Case "En Cours"
                                Type_Pres.Cour = Type_Pres.Cour + 1
                                
                            Case "Non Commencé"
                                Type_Pres.NotYet = Type_Pres.NotYet + 1
                                
                        End Select
                        
                        '**************
                        'les résultats
                        '**************
                        Select Case extract(y, 12)
                            Case "Réussi"
                                Type_Pres.Reussi = Type_Pres.Reussi + 1
                                    
                            Case "En attente"
                                Type_Pres.enAttente = Type_Pres.enAttente + 1
                                    
                            Case "Echec"
                                Type_Pres.Echec = Type_Pres.Echec + 1
                                    
                            Case "Annulé"
                                Type_Pres.Annule = Type_Pres.Annule + 1
                                    
                            Case "Révisé"
                                Type_Pres.Revise = Type_Pres.Revise + 1
                                                                
                        End Select

                        
                    Case "ACH"
                        type_chasse.Mission = type_chasse.Mission + 1
                        'Type_Chasse.Projet = Type_Chasse.Projet + Sheets("extract").Range("H" & y)
                    
                        If Not extract(y, 11) = "" Then
                    
                            Moy_Proj.MoyChasse = Moy_Proj.MoyChasse + CInt(extract(y, 11))
                            Moy_Jour.jourChasse = Moy_Jour.jourChasse + CInt(extract(y, 8))
                         
                        End If
                    
                        Select Case extract(y, 10)
                            Case ""
                                type_chasse.Vide = type_chasse.Vide + 1
                                
                            Case "Terminer"
                                type_chasse.Done = type_chasse.Done + 1
                                
                            Case "stand by"
                                type_chasse.StandBy = type_chasse.StandBy + 1
                                
                            Case "Clôturer"
                                type_chasse.Cloture = type_chasse.Cloture + 1
                                
                            Case "En Cours"
                                type_chasse.Cour = type_chasse.Cour + 1
                                
                            Case "Non Commencé"
                                type_chasse.NotYet = type_chasse.NotYet + 1
                                
                        End Select
                        
                        '**************
                        'les résultats
                        '**************
                        Select Case extract(y, 12)
                            Case "Réussi"
                                type_chasse.Reussi = type_chasse.Reussi + 1
                                    
                            Case "En attente"
                                type_chasse.enAttente = type_chasse.enAttente + 1
                                    
                            Case "Echec"
                                type_chasse.Echec = type_chasse.Echec + 1
                                    
                            Case "Annulé"
                                type_chasse.Annule = type_chasse.Annule + 1
                                    
                            Case "Révisé"
                                type_chasse.Revise = type_chasse.Revise + 1
                                                                
                        End Select
                        
                    Case "APKS"
                        Type_Preks.Mission = Type_Preks.Mission + 1
                        'Type_Preks.Projet = Type_Preks.Projet + Sheets("extract").Range("H" & y)
                    
                        If Not extract(y, 11) = "" Then
                    
                            Moy_Proj.MoyPreks = Moy_Proj.MoyPreks + CInt(extract(y, 11))
                            Moy_Jour.jourPreks = Moy_Jour.jourPreks + CInt(extract(y, 8))
                         
                        End If
                    
                        Select Case extract(y, 10)
                            Case ""
                                Type_Preks.Vide = Type_Preks.Vide + 1
                                
                            Case "Terminer"
                                Type_Preks.Done = Type_Preks.Done + 1
                                
                            Case "stand by"
                                Type_Preks.StandBy = Type_Preks.StandBy + 1
                                
                            Case "Clôturer"
                                Type_Preks.Cloture = Type_Preks.Cloture + 1
                                
                            Case "En Cours"
                                Type_Preks.Cour = Type_Preks.Cour + 1
                                
                            Case "Non Commencé"
                                Type_Preks.NotYet = Type_Preks.NotYet + 1
                                
                        End Select
                        
                        '**************
                        'les résultats
                        '**************
                        Select Case extract(y, 12)
                            Case "Réussi"
                                Type_Preks.Reussi = Type_Preks.Reussi + 1
                                    
                            Case "En attente"
                                Type_Preks.enAttente = Type_Preks.enAttente + 1
                                    
                            Case "Echec"
                                Type_Preks.Echec = Type_Preks.Echec + 1
                                    
                            Case "Annulé"
                                Type_Preks.Annule = Type_Preks.Annule + 1
                                    
                            Case "Révisé"
                                Type_Preks.Revise = Type_Preks.Revise + 1
                                                                
                        End Select
                        
                    Case "AMD"
                        Type_AMD.Mission = Type_AMD.Mission + 1
                        'Type_AMD.Projet = Type_AMD.Projet + Sheets("extract").Range("H" & y)
                    
                        If Not extract(y, 11) = "" Then
                    
                            Moy_Proj.MoyAmd = Moy_Proj.MoyAmd + CInt(extract(y, 11))
                            Moy_Jour.jourAmd = Moy_Jour.jourAmd + CInt(extract(y, 8))
                            Debug.Print Moy_Proj.MoyAmd
                        End If
                    
                        '**************
                        'total des état
                        '**************
                    
                        Select Case extract(y, 10)
                            Case ""
                                Type_AMD.Vide = Type_AMD.Vide + 1
                                
                            Case "Terminer"
                                Type_AMD.Done = Type_AMD.Done + 1
                                
                            Case "stand by"
                                Type_AMD.StandBy = Type_AMD.StandBy + 1
                                
                            Case "Clôturer"
                                Type_AMD.Cloture = Type_AMD.Cloture + 1
                                
                            Case "En Cours"
                                Type_AMD.Cour = Type_AMD.Cour + 1
                                
                            Case "Non Commencé"
                                Type_AMD.NotYet = Type_AMD.NotYet + 1
                                
                        End Select
                        
                        '**************
                        'les résultats
                        '**************
                        Select Case extract(y, 12)
                            Case "Réussi"
                                Type_AMD.Reussi = Type_AMD.Reussi + 1
                                    
                            Case "En attente"
                                Type_AMD.enAttente = Type_AMD.enAttente + 1
                                    
                            Case "Echec"
                                Type_AMD.Echec = Type_AMD.Echec + 1
                                    
                            Case "Annulé"
                                Type_AMD.Annule = Type_AMD.Annule + 1
                                    
                            Case "Révisé"
                                Type_AMD.Revise = Type_AMD.Revise + 1
                                                                
                        End Select
                    
                End Select
                
                '************************************************
                'calculer le nombre de chaque résultat de mission
                '************************************************
              
                Select Case extract(y, 12)
              
                    Case "Réussi"
                        Result.Reussi = Result.Reussi + 1
                        
                    Case "En attente"
                        Result.enAttente = Result.enAttente + 1
    
                    Case "Annulé"
                        Result.Annule = Result.Annule + 1
                   
                    Case "Echec"
                        Result.Echec = Result.Echec + 1
                   
                    Case "Révisé"
                        Result.Revise = Result.Revise + 1
                        
                End Select
               
               
                '*********************************
                'calculer le nombre de chaque état
                '*********************************
              
              
                etat_Mission = extract(y, 10)
                'Debug.Print etat_Mission
                Select Case etat_Mission
                    Case "Terminer"
                        etat_Terminer = etat_Terminer + 1
                    Case "Non Commencé"
                        etat_NonCommencer = etat_NonCommencer + 1
                    Case "stand by"
                        etat_StundBy = etat_StundBy + 1
                    Case "Clôturer"
                        etat_Cloturer = etat_Cloturer + 1
                    Case "En Cours"
                        etat_EnCour = etat_EnCour + 1
                    Case ""
                        etat_NotEdit = etat_NotEdit + 1
                      
                End Select
              
                'on compte le nombre de mission par type
             
            End If
          
        Next
     
        'on remplit
        'stat global
     
   


        On Error Resume Next
        Sheets("Synthése Générale HH").Select

        'ActiveSheet.Range("C3").Value = nbrProjet
        'ActiveSheet.Range("C2").Value = nbrMission
        'ActiveSheet.Range("C4").Value = tabDesDonnes(0, 1) 'tabDesDonnes(0, 1)
        ' ActiveSheet.Range("C5").Value = Type_Preks.Mission
        'ActiveSheet.Range("C6").Value = Type_Prekc.Mission
        'ActiveSheet.Range("C7").Value = Type_Pres.Mission
        'ActiveSheet.Range("C8").Value = type_chasse.Mission
        'ActiveSheet.Range("C9").Value = Type_AMD.Mission
     
        'stat etat
     
        'ActiveSheet.Range("H2").Value = etat_NonCommencer
        'ActiveSheet.Range("H3").Value = etat_EnCour
        'ActiveSheet.Range("H4").Value = etat_Terminer
        'ActiveSheet.Range("H5").Value = etat_Cloturer
        'ActiveSheet.Range("H6").Value = etat_StundBy
     
        'on recherche le tableau s'il a été déplacé ...
        'On Error GoTo errorHandler

        'Cells.Find(What:="Tri des CV", After:=ActiveCell, LookIn:=xlFormulas, _
        'LookAt:=xlPart, SearchOrder:=xlByRows, SearchDirection:=xlNext, _
        'MatchCase:=False, SearchFormat:=False).Activate
        
        'errorHandler:

        'If Err.Number = 91 Then
        ' MsgBox "Veuillez indiquer le tableau de destination en indiquant la 1er colonne " & "" & "ID Mission"
        'Exit Sub
        'End If

If annTrait(n) = "janvier" Then
            Range("D10").Select
        
End If
If annTrait(n) = "février" Then
            Range("E10").Select
        
End If
If annTrait(n) = "mars" Then
            Range("F10").Select
        
End If
If annTrait(n) = "avril" Then
            Range("G10").Select
        
End If
If annTrait(n) = "mai" Then
            Range("H10").Select
        
End If
If annTrait(n) = "juin" Then
            Range("I10").Select
        
End If
If annTrait(n) = "juillet" Then
            Range("J10").Select
        
End If
If annTrait(n) = "août" Then
            Range("K10").Select
        
End If
If annTrait(n) = "septembre" Then
            Range("L10").Select
        
End If
If annTrait(n) = "octobre" Then
            Range("M10").Select
        
End If
If annTrait(n) = "novembre" Then
            Range("N10").Select
        
End If
If annTrait(n) = "décembre" Then
            Range("O10").Select
        
End If

        ActiveCell.Value = nbrProjet
    
        ActiveCell.Offset(1, 0).Value = nbrMission
        'ActiveCell.Offset(18, 0).Value = tabDesDonnes(0, 1) 'tabDesDonnes(0, 1)
        'ActiveCell.Offset(19, 0).Value = Type_Preks.Mission
        'ActiveCell.Offset(20, 0).Value = Type_Prekc.Mission
        'ActiveCell.Offset(21, 0).Value = Type_Pres.Mission
        'ActiveCell.Offset(22, 0).Value = type_chasse.Mission
        'ActiveCell.Offset(23, 0).Value = Type_AMD.Mission
     
        'stat etat
     
        'ActiveCell.Offset(16, 3).Value = etat_NonCommencer
        'ActiveCell.Offset(17, 3).Value = etat_EnCour
        'ActiveCell.Offset(18, 3).Value = etat_Terminer
        'ActiveCell.Offset(19, 3).Value = etat_Cloturer
        'ActiveCell.Offset(20, 3).Value = etat_StundBy

        'stat détails
        
       ' ActiveCell.Value = tabDesDonnes(0, 1)
       ' ActiveCell.Offset(0, 1).Value = Type_Preks.Mission
       ' ActiveCell.Offset(0, 2).Value = Type_Prekc.Mission
       ' ActiveCell.Offset(0, 3).Value = Type_Pres.Mission
      '  ActiveCell.Offset(0, 4).Value = type_chasse.Mission
       ' ActiveCell.Offset(0, 5).Value = Type_AMD.Mission
     
       ' ActiveCell.Offset(1, 0).Value = Type_Tri.Projet
       ' ActiveCell.Offset(1, 1).Value = Type_Preks.Projet
        'ActiveCell.Offset(1, 2).Value = Type_Prekc.Projet
        'ActiveCell.Offset(1, 3).Value = Type_Pres.Projet
        'ActiveCell.Offset(1, 4).Value = type_chasse.Projet
        'ActiveCell.Offset(1, 5).Value = Type_AMD.Projet
     
       ' ActiveCell.Offset(6, 0).Value = Type_Tri.Vide
       ' ActiveCell.Offset(6, 1).Value = Type_Preks.Vide
       ' ActiveCell.Offset(6, 2).Value = Type_Prekc.Vide
        'ActiveCell.Offset(6, 3).Value = Type_Pres.Vide
       ' ActiveCell.Offset(6, 4).Value = type_chasse.Vide
       ' ActiveCell.Offset(6, 5).Value = Type_AMD.Vide


       ' ActiveCell.Offset(2, 0).Value = Type_Tri.Cour
        'ActiveCell.Offset(2, 1).Value = Type_Preks.Cour
       ' ActiveCell.Offset(2, 2).Value = Type_Prekc.Cour
        'ActiveCell.Offset(2, 3).Value = Type_Pres.Cour
        'ActiveCell.Offset(2, 4).Value = type_chasse.Cour
       ' ActiveCell.Offset(2, 5).Value = Type_AMD.Cour
     
       ' ActiveCell.Offset(3, 0).Value = Type_Tri.Done
        'ActiveCell.Offset(3, 1).Value = Type_Preks.Done
        'ActiveCell.Offset(3, 2).Value = Type_Prekc.Done
        'ActiveCell.Offset(3, 3).Value = Type_Pres.Done
       ' ActiveCell.Offset(3, 4).Value = type_chasse.Done
       ' ActiveCell.Offset(3, 5).Value = Type_AMD.Done
     
       ' ActiveCell.Offset(4, 0).Value = Type_Tri.StandBy
       ' ActiveCell.Offset(4, 1).Value = Type_Preks.StandBy
       ' ActiveCell.Offset(4, 2).Value = Type_Prekc.StandBy
       ' ActiveCell.Offset(4, 3).Value = Type_Pres.StandBy
       ' ActiveCell.Offset(4, 4).Value = type_chasse.StandBy
        'ActiveCell.Offset(4, 5).Value = Type_AMD.StandBy
     
        'ActiveCell.Offset(5, 0).Value = Type_Tri.NotYet
       ' ActiveCell.Offset(5, 1).Value = Type_Preks.NotYet
      '  ActiveCell.Offset(5, 2).Value = Type_Prekc.NotYet
      '  ActiveCell.Offset(5, 3).Value = Type_Pres.NotYet
       ' ActiveCell.Offset(5, 4).Value = type_chasse.NotYet
       ' ActiveCell.Offset(5, 5).Value = Type_AMD.NotYet
     
       ' ActiveCell.Offset(7, 0).Value = Type_Tri.Cloture
        'ActiveCell.Offset(7, 1).Value = Type_Preks.Cloture
        'ActiveCell.Offset(7, 2).Value = Type_Prekc.Cloture
        'ActiveCell.Offset(7, 3).Value = Type_Pres.Cloture
       ' ActiveCell.Offset(7, 4).Value = type_chasse.Cloture
        'ActiveCell.Offset(7, 5).Value = Type_AMD.Cloture
     
'        ActiveCell.Offset(16, 5).Value = Result.Reussi
'        ActiveCell.Offset(17, 5).Value = Result.enAttente
'        ActiveCell.Offset(18, 5).Value = Result.Annule
'        ActiveCell.Offset(19, 5).Value = Result.Echec
'        ActiveCell.Offset(20, 5).Value = Result.Revise
'        'mission réussite
'        ActiveCell.Offset(9, 0).Value = Type_Tri.Reussi & " " & "(" & Format((Type_Tri.Reussi / tabDesDonnes(0, 1)) * 100, "##") & "%)"
'        ActiveCell.Offset(9, 1).Value = Type_Preks.Reussi & " " & "(" & Format((Type_Preks.Reussi / Type_Preks.Mission) * 100, "##") & "%)"
'        ActiveCell.Offset(9, 2).Value = Type_Prekc.Reussi & " " & "(" & Format((Type_Prekc.Reussi / Type_Prekc.Mission) * 100, "##") & "%)"
'        ActiveCell.Offset(9, 3).Value = Type_Pres.Reussi & " " & "(" & Format((Type_Pres.Reussi / Type_Pres.Mission) * 100, "##") & "%)"
'        ActiveCell.Offset(9, 4).Value = type_chasse.Reussi & " " & "(" & Format((type_chasse.Reussi / type_chasse.Mission) * 100, "##") & "%)"
'        ActiveCell.Offset(9, 5).Value = Type_AMD.Reussi & " " & "(" & Format((Type_AMD.Reussi / Type_AMD.Mission) * 100, "##") & "%)"
'
'        ActiveCell.Offset(10, 0).Value = Type_Tri.enAttente
'        ActiveCell.Offset(10, 1).Value = Type_Preks.enAttente
'        ActiveCell.Offset(10, 2).Value = Type_Prekc.enAttente
'        ActiveCell.Offset(10, 3).Value = Type_Pres.enAttente
'        ActiveCell.Offset(10, 4).Value = type_chasse.enAttente
'        ActiveCell.Offset(10, 5).Value = Type_AMD.enAttente
'
'        ActiveCell.Offset(11, 0).Value = Type_Tri.Annule
'        ActiveCell.Offset(11, 1).Value = Type_Preks.Annule
'        ActiveCell.Offset(11, 2).Value = Type_Prekc.Annule
'        ActiveCell.Offset(11, 3).Value = Type_Pres.Annule
'        ActiveCell.Offset(11, 4).Value = type_chasse.Annule
'        ActiveCell.Offset(11, 5).Value = Type_AMD.Annule
'
'        ActiveCell.Offset(12, 0).Value = Type_Tri.Echec & " " & "(" & Format((Type_Tri.Echec / tabDesDonnes(0, 1)) * 100, "##") & "%)"
'        ActiveCell.Offset(12, 1).Value = Type_Preks.Echec & " " & "(" & Format((Type_Preks.Echec / Type_Preks.Mission) * 100, "##") & "%)"
'        ActiveCell.Offset(12, 2).Value = Type_Prekc.Echec & " " & "(" & Format((Type_Prekc.Echec / Type_Prekc.Mission) * 100, "##") & "%)"
'        ActiveCell.Offset(12, 3).Value = Type_Pres.Echec & " " & "(" & Format((Type_Pres.Echec / Type_Pres.Mission) * 100, "##") & "%)"
'        ActiveCell.Offset(12, 4).Value = type_chasse.Echec & " " & "(" & Format((type_chasse.Echec / type_chasse.Mission) * 100, "##") & "%)"
'        ActiveCell.Offset(12, 5).Value = Type_AMD.Echec & " " & "(" & Format((Type_AMD.Echec / Type_AMD.Mission) * 100, "##") & "%)"
'
'        ActiveCell.Offset(13, 0).Value = Type_Tri.Revise
'        ActiveCell.Offset(13, 1).Value = Type_Preks.Revise
'        ActiveCell.Offset(13, 2).Value = Type_Prekc.Revise
'        ActiveCell.Offset(13, 3).Value = Type_Pres.Revise
'        ActiveCell.Offset(13, 4).Value = type_chasse.Revise
'        ActiveCell.Offset(13, 5).Value = Type_AMD.Revise



'        On Error Resume Next
'
'        If Not Moy_Proj.MoyTri = 0 Then
'            ActiveCell.Offset(8, 0).Value = Format(Moy_Proj.MoyTri / Moy_Jour.jourTri, "##")
'        Else
'            ActiveCell.Offset(8, 0).Value = 0
'        End If
'
'        If Not Moy_Proj.MoyPreks = 0 Then
'            ActiveCell.Offset(8, 1).Value = Format(Moy_Proj.MoyPreks / Moy_Jour.jourPreks, "##")
'        Else
'            ActiveCell.Offset(8, 1).Value = 0
'        End If
'        If Not Moy_Proj.MoyPrekc = 0 Then
'            ActiveCell.Offset(8, 2).Value = Format(Moy_Proj.MoyPrekc / Moy_Jour.jourPrekc, "##")
'        Else
'            ActiveCell.Offset(8, 2).Value = 0
'        End If
'        If Not Moy_Proj.MoyPres = 0 Then
'            ActiveCell.Offset(8, 3).Value = Format(Moy_Proj.MoyPres / Moy_Jour.jourPres, "##")
'        Else
'            ActiveCell.Offset(8, 3).Value = 0
'        End If
'        If Not Moy_Proj.MoyChasse = 0 Then
'            ActiveCell.Offset(8, 4).Value = Format(Moy_Proj.MoyChasse / Moy_Jour.jourChasse, "##")
'        Else
'            ActiveCell.Offset(8, 4).Value = 0
'        End If
'        If Not Moy_Proj.MoyAmd = 0 Then
'            ActiveCell.Offset(8, 5).Value = Format(Moy_Proj.MoyAmd / Moy_Jour.jourAmd, "##")
'        Else
'            ActiveCell.Offset(8, 5).Value = 0
'        End If
     
    Next
End Sub

Function Restart()
'On Error Resume Next

    'tabDesDonnes(0, 1) = 0
    Type_Prekc.Mission = 0
    Type_Pres.Mission = 0
    type_chasse.Mission = 0
    Type_Preks.Mission = 0
    Type_Tri.Projet = 0
    Type_Pres.Projet = 0
    Type_Preks.Projet = 0
    Type_Prekc.Projet = 0
    type_chasse.Projet = 0
    Type_AMD.Mission = 0
    Type_AMD.Projet = 0
    Type_Tri.Vide = 0
    Type_Preks.Vide = 0
    Type_Prekc.Vide = 0
    Type_Pres.Vide = 0
    type_chasse.Vide = 0
    Type_AMD.Vide = 0
    Type_Tri.Cour = 0
    Type_Preks.Cour = 0
    Type_Prekc.Cour = 0
    Type_Pres.Cour = 0
    type_chasse.Cour = 0
    Type_AMD.Cour = 0
    Type_Tri.Done = 0
    Type_Preks.Done = 0
    Type_Prekc.Done = 0
    Type_Pres.Done = 0
    type_chasse.Done = 0
    Type_AMD.Done = 0
    Type_Tri.StandBy = 0
    Type_Preks.StandBy = 0
    Type_Prekc.StandBy = 0
    Type_Pres.StandBy = 0
    type_chasse.StandBy = 0
    Type_AMD.StandBy = 0
    Type_Tri.NotYet = 0
    Type_Preks.NotYet = 0
    Type_Prekc.NotYet = 0
    Type_Pres.NotYet = 0
    type_chasse.NotYet = 0
    Type_AMD.NotYet = 0
    Type_Tri.Cloture = 0
    Type_Preks.Cloture = 0
    Type_Prekc.Cloture = 0
    Type_Pres.Cloture = 0
    type_chasse.Cloture = 0
    Type_AMD.Cloture = 0
    Moy_Proj.MoyAmd = 0
    Moy_Proj.MoyChasse = 0
    Moy_Proj.MoyPrekc = 0
    Moy_Proj.MoyPreks = 0
    Moy_Proj.MoyPres = 0
    Moy_Proj.MoyTri = 0
    Moy_Jour.jourTri = 0
    Moy_Jour.jourPreks = 0
    Moy_Jour.jourPrekc = 0
    Moy_Jour.jourPres = 0
    Moy_Jour.jourChasse = 0
    Moy_Jour.jourAmd = 0
    Result.Reussi = 0
    Result.Annule = 0
    Result.enAttente = 0
    Result.Echec = 0
    Result.Revise = 0
    Type_Tri.Reussi = 0
    Type_Preks.Reussi = 0
    Type_Prekc.Reussi = 0
    Type_Pres.Reussi = 0
    type_chasse.Reussi = 0
    Type_AMD.Reussi = 0
    Type_Tri.Echec = 0
    Type_Preks.Echec = 0
    Type_Prekc.Echec = 0
    Type_Pres.Echec = 0
    type_chasse.Echec = 0
    Type_AMD.Echec = 0
    Type_Tri.enAttente = 0
    Type_Preks.enAttente = 0
    Type_Prekc.enAttente = 0
    Type_Pres.enAttente = 0
    type_chasse.enAttente = 0
    Type_AMD.enAttente = 0
    Type_Tri.Revise = 0
    Type_Preks.Revise = 0
    Type_Prekc.Revise = 0
    Type_Pres.Revise = 0
    type_chasse.Revise = 0
    Type_AMD.Revise = 0
    Type_Tri.Annule = 0
    Type_Preks.Annule = 0
    Type_Prekc.Annule = 0
    Type_Pres.Annule = 0
    type_chasse.Annule = 0
    Type_AMD.Annule = 0
  



End Function




